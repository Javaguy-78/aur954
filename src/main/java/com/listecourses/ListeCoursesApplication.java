package com.listecourses;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ListeCoursesApplication {
    public static void main(String[] args) {
        SpringApplication.run(ListeCoursesApplication.class, args);
    }
}
